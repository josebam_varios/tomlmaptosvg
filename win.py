# This Python file uses the following encoding: utf-8
import sys

from tomlReader import tomlReader

from PySide2.QtWidgets import QApplication, QMainWindow, QToolBar
from PySide2.QtUiTools import QUiLoader
from PySide2.QtCore import QFile
from PySide2.QtGui import QIcon
from PySide2.QtSvg import QSvgWidget


if __name__ == "__main__":
    app = QApplication(sys.argv)

#    iu window
#    ui_file = QFile("mainwindow.ui")
#    ui_file.open(QFile.ReadOnly)

#    loader = QUiLoader()
#    window = loader.load(ui_file)
#    ui_file.close()

    window = QMainWindow()

    tomlF = tomlReader()
    tomlF.read()
    tomlF.toSVG()

    image = QSvgWidget('out.svg', window)

    window.setCentralWidget(image)

    window.show()

    sys.exit(app.exec_())
