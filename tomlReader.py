# This Python file uses the following encoding: utf-8

import toml
import math


class tomlReader:
    """ Toml Reader that generates SVG map file """

#   Category Subclass
    class __Category:
        """Category class"""

        def __init__(self):
            self.cat = None
            self.pairs = []

        def check(self):
            return (self.cat is not None
                    and self.pairs != [(None, (None, None))])

        def checkTupla(self, x):
            ret = (x[0] is None)
            ret = (x[1][0] is None)
            ret = (x[1][1] is None)
            return not ret

        def app(self, x):
            if self.checkTupla(x):
                self.pairs.append(x)

        def printIt(self):
            print('\n--------------------------')
            print('\t', self.cat, sep=' ')
            for i in self.pairs:
                print('L |', i, sep=' ')
            print()

#   Main Class methods definition
    def __init__(self):
        self.readFile = "/home/joseba/projects/Toml_Map/tomlmaptosvg/m.toml"
        self.outputFile = \
            "/home/joseba/projects/Toml_Map/tomlmaptosvg/m.toml"
        self.items = []
        self.rawData = None

        # Drawing constants
        self.pointScale = 0.003  # from full size

    def printIt(self):
        for i in self.items:
            i.printIt()

    def read(self):
        input = toml.load(self.readFile)
        self.rawData = input
        for i in input:
            if i is not None:
                item = self.__Category()
                item.cat = i
                input_1 = input.get(i)

                for j in input_1:
                    tmp = input_1.get(j)
                    x = tmp[0]
                    y = tmp[1]
                    tmp_tup = (j, (x, y))
                    item.app(tmp_tup)

                if len(item.pairs) > 0:
                    self.items.append(item)

    def xMin(self):
        ret = 0
        for j in self.items:
            for i in j.pairs:
                if i[1][0] < ret:
                    ret = i[1][0]
        return ret

    def yMin(self):
        ret = 0
        for j in self.items:
            for i in j.pairs:
                if i[1][1] < ret:
                    ret = i[1][1]
        return ret

    def xMax(self):
        ret = 0
        for j in self.items:
            for i in j.pairs:
                if i[1][0] > ret:
                    ret = i[1][0]
        return ret

    def yMax(self):
        ret = 0
        for j in self.items:
            for i in j.pairs:
                if i[1][1] > ret:
                    ret = i[1][1]
        return ret

    def output(self):
        o = open(self.outputFile, 'w')
        toml.dump(self.rawData, o)

    def toSVG(self):
        X_2 = math.floor((self.xMax()+self.xMin())/2)
        Y_2 = math.floor((self.yMax()+self.yMin())/2)
        xMin = self.xMin()
        yMin = self.yMin()
        xMax = self.xMax()
        yMax = self.yMax()
        dx = xMax-xMin
        dy = yMax-yMin
        pRad = math.floor(abs(max(dx, dy)*self.pointScale))
        dx += 50*pRad
        dy += 50*pRad
        if xMin < 0:
            xOff = abs(xMin)+1
        else:
            xOff = 0
        if xMin < 0:
            yOff = abs(yMin)+1
        else:
            yOff = 0

        # Write headers
        f = open(self.outputFile, 'w')
        f.write('<?xml version="1.0" standalone="no"?>\n')

        line = ('<svg width="100%" height="100%" viewBox="0 0 ' + str(dx)
                + ' ' + str(dy) + '" xmlns="http://www.w3.org/2000/svg" '
                + 'version="1.1">\n\n')
        f.write(line)

        # Write grid and aux data

        # Write points
        for j in self.items:
            # Random color for each category

            # Write points with labels and coordinates
            for i in j.pairs:
                X = i[1][0]+xOff
                Y = i[1][1]+yOff
                # EX: <circle cx="50" cy="50" r="40" stroke="black" \
                # stroke-width="3" fill="red" />
                line = ('<circle cx="' + str(X) + '" cy="' + str(Y) + '" r="'
                        + str(pRad)+'" stroke="black" stroke-width="1" '
                        + 'fill="red" />\n')
                f.write(line)
                # EX: <text x="20" y="20" font-family="sans-serif" \
                # font-size="20px" fill="red">Hello!</text>
                line = ('<text x="' + str(X + pRad + 1)+'" y="'
                        + str(Y-pRad*5-1) + '" font-family="sans-serif"'
                        + ' font-size="' + str(pRad * 4) + 'px" fill="red">'
                        + str(i[0]) + '</text>\n')
                f.write(line)
                line = ('<text x="' + str(X + pRad*4 + 1) + '" y="' + str(Y)
                        + '" font-family="sans-serif" font-size="'
                        + str(pRad * 4) + 'px" fill="red">(X: ' + str(i[1][0])
                        + ', Y: ' + str(i[1][1]) + ')</text>\n\n')
                f.write(line)

        # Write Aux data

        # Close file
        f.write('</svg>\n')
        f.close()
