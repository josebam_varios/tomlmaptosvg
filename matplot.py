from tomlReader import tomlReader

import matplotlib.pyplot as plt
import numpy as np

data = tomlReader()
data.read()
data.output()

np.random.seed()

x = []
y = []
area = []
colors = []
fig = plt.figure()
ax = fig.add_subplot(111)

for j in data.items:
    rnd_color = np.random.rand()
    for i in j.pairs:
        colors.append(rnd_color)
        area.append(3.1416*(4**2))
        x.append(i[1][0])
        y.append(i[1][1])
        ax.annotate(f"({i[1][0]}, {i[1][1]})", xy=(i[1][0], i[1][1]), textcoords='data')

xy = zip(x, y)
plt.scatter(x, y, s=area, c=colors, alpha=0.5)

plt.show()
